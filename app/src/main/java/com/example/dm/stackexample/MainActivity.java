package com.example.dm.stackexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity{

    String item;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText editText=(EditText)findViewById(R.id.editxt);
        ListView listView=(ListView)findViewById(R.id.listview);
        final ArrayList<String> todoItems = new ArrayList<String>();
        final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, todoItems);
        listView.setAdapter(adapter);


        Button btn=(Button)findViewById(R.id.push);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().isEmpty()

                        ) {
                    Toast.makeText(MainActivity.this, "Please enter element", Toast.LENGTH_SHORT).show();
                } else {
                    item = editText.getText().toString();
                    todoItems.add(item);
                    adapter.notifyDataSetChanged();
                    editText.getText().clear();
                }
            }
        });

        Button btn1=(Button)findViewById(R.id.pop);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (todoItems.size()==0) {
                    Toast.makeText(MainActivity.this, "Stack is Empty", Toast.LENGTH_SHORT).show();
                } else {
                    todoItems.remove(0);
                    adapter.notifyDataSetChanged();
                }
            }


        });
        }

}
